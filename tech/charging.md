# USB・無線充電規格

乱立する充電規格を整理したものです。体系的にすべてまとまっているサイトが見当たらなかったので作りました。残念ながら、今もなおUSB規格を無視した新しい規格が乱立している状況のため、随時更新していきます。

最終更新: 2022/03/04

> 訂正や追加情報がありましたら、[ぜひ私にご連絡ください](https://rintan.net)。表中の空欄は不足している情報です。
> 
> また、この表は[GitLab](https://gitlab.com/Rintan/docs)上でオープンソースとして管理されています。IssueやPull Requestは歓迎します。

## 目次
> - [USB-IF](#usb-if)
>    - [Default USB Power](#default-usb-power)
>    - [USB-BC](#usb-bc)
>    - [USB Type-C Current](#usb-type-c-current)
>    - [USB Power Delivery](#usb-power-delivery)
> - [PoweredUSB](#poweredusb)
> - [Qi](#qi)
> - [Qualcomm Quick Charge](#qualcomm-quick-charge)
> - [MediaTek Pump Express](#mediatek-pump-express)
> - [Apple](#apple)
> - [Huawei](#huawei)
> - [Samsung](#samsung)
> - [Xiaomi](#xiaomi)
> - [ASUS](#asus)
> - [Motolola TurboPower](#motolola-turbopower)
> - [OPPO VOOC](#oppo-vooc-voltage-open-loop-multi-step-constant-current-charging)
> - [realme Dart](#realme-dart)
> - [OnePlus](#oneplus)
> - [vivo](#vivo)

## USB-IF

### Default USB Power
| 名称              | 電圧 | 電流  | 電力 |
|:-----------------:|:----:|:-----:|:----:|
| USB Power 1.x/2.0 | 5V   | 500mA | 2.5W |
| USB Power 3.x     | 5V   | 900mA | 4.5W |

### USB-BC
USB-BC (Battery Charging) Rev. 1.2に基づく

| 名称                                     | 電圧 | 電流      | 電力     |
|:----------------------------------------:|:----:|:---------:|:--------:|
| SDP (Standard Downstream Port) + USB 2.0 | 5V   | 100~500mA | 0.5~2.5W |
| SDP + USB 3.x                            | 5V   | 900mA     | 4.5W     |
| DCP (Dedicated Charging Port)            | 5V   | 0.5~5A    | 2.5~25W  |
| CDP (Charging Downstream Port)           | 5V   | 1.5~5A    | 7.5~25W  |

### USB Type-C Current
| 電圧 | 電流 | 電力 |
|:----:|:----:|:----:|
| 5V   | 1.5A | 7.5W |
| 5V   | 3A   | 15W  |

### USB Power Delivery
2021/05/25発表のUSB Type-C Specification Revision 2.1に基づく

| バージョン | 電圧 | 電流 | 電力     |
|:----------:|:----:|:----:|:--------:|
| PD 2.0以上 | 5V   | 3A   | 0~15W    |
| PD 2.0以上 | 9V   | 3A   | 15~27W   |
| PD 2.0以上 | 12V  | 2.5A | 30W      |
| PD 2.0以上 | 15V  | 3A   | 27~45W   |
| PD 2.0以上 | 20V  | 3A   | 45~60W   |
| PD 2.0以上 | 20V  | 5A   | 60~100W  |
| PD 3.1以上 | 48V  | 5A   | 100~240W |

※1 12V 2.5A 30Wの出力はオプション扱い  
※2 電力の幅表記は、USB PD Revision 3.0以上でPPS (Programable Power Supply)に対応している場合  
※3 PPSでは100mV・50mA刻みで電圧・電流が変化する  
※4 100W以上の電力供給にはEPR (Extended Power Range)のサポートが必須

## PoweredUSB
別名: Retail USB, USB +Power  
IBMが開発

| 名称            | 電圧     | 電流 | 電力 | 発表    |
|:---------------:|:--------:|:----:|:----:|:-------:|
| PoweredUSB 0.8g | 5/12/24V | ~6A  |      | 2004.01 |

## Qi
| バージョン                          | 電力 | 発表    |
|:-----------------------------------:|:----:|:-------:|
| Qi 1.0                              | 5W   | 2010.07 |
| Qi 1.1                              | 5W   | 2012.03 |
| Qi 1.2 BPP (Baseline Power Profile) | 5W   | 2015.10 |
| Qi 1.2 EPP (Extended Power Profile) | 15W  | 2015.10 |

## Qualcomm Quick Charge
| バージョン     | 電圧             | 電流      | 電力     | 下位互換 | PD互換 | 発表    |
|:--------------:|:----------------:|:---------:|:--------:|:--------:|:------:|:-------:|
| QC 1.0         | 5V               | 1.5A      | 10W      | 〇       | ×     | 2012.06 |
| QC 2.0 Class A | 5/9/12V          | 1.5/3A    | ~18W     | 〇       | ×     | 2013.02 |
| QC 2.0 Class B | 5/9/12/20V       | 2A        | ~40W     | 〇       | ×     | 2013.02 |
| QC 3.0         | 3.6~20V (#200mV) | 2.6/4.6A  | ~18W     | 〇       | ×     | 2015.09 |
| QC 3+          | (#200mV)         |           |          | 〇       | 〇     | 2020.04 |
| QC 4           | 3.3~20V (#20mV)  | 2.6/4.6A  | ~27W     | ×       | 〇     | 2016.11 |
| QC 4+          | 3.3~20V          | 2.6/4.6A  | ~27W     | 〇       | 〇     | 2017.06 |
| QC 5           | 3.3~20V+         | 3/5A, 5A~ | 45~100W+ | 〇       | 〇     | 2020.07 |

## MediaTek Pump Express

### 有線
| バージョン | 電圧           | 電流   | 電力   |
|:----------:|:--------------:|:------:|:------:|
| PE+        | 5/7/9/12V      | 3/4.5A | 15~54W |
| PE+ 2.0    | 5~20V (#500mV) | 3/4.5A | 15~90W |
| PE 3.0     | 3~6V (#10mV)   | 5A     | 15~30W |
| PE 4.0     |                |        |        |

※ PE 4.0は[PD PPS](#usb-power-delivery)と互換性あり

### 無線
| 名称      　| 電力   |
|:-----------:|:------:|
| PE Wireless | 0~15W  |

※ [Qi 1.2.3 EPP](#qi)と互換性あり

## Apple

### 有線
| 電圧 | 電流 | 電力 | 機種         |
|:----:|:----:|:----:|:------------:|
| 5V   | 1A   | 5W   | 全てのiPhone |
| 5V   | 2.1A | 10W  | iPhone 6~    |
| 5V   | 2.4A | 12W  | iPhone 7~    |

※1 ケーブルはLightningケーブル  
※2 iPhone 8/X以降はUSB PDプロトコル

### 無線
iPhone 12以降向け無線充電

| 名称    | 電力 |
|:-------:|:----:|
| MagSafe | ~15W |

## Huawei

### 有線
| 名称                                      | 電圧 | 電流 | 電力  |
|:-----------------------------------------:|:----:|:----:|:-----:|
| FCP (Fast Charger Protocol)               | 9V   | 2A   | 18W   |
| SCP (Smart Charging Protocol/SuperCharge) | 5V   | 4.5A | 22.5W |
| SCP                                       | 4.5V | 5A   | 22.5W |
| SCP                                       | 10V  | 4A   | 40W   |
| SCP                                       | 5V   | 8A   | 40W   |

### 無線
| 名称         | 電力 |
|:------------:|:----:|
| Wireless SCP | ~27W |

## Samsung

### 有線
| 名称                         | 電圧 | 電流 | 電力   | 互換                            |
|:----------------------------:|:----:|:----:|:------:|:-------------------------------:|
| AFC (Adaptive Fast Charging) | 5/9V | 2A   | 10/18W | [QC2.0](#qualcomm-quick-charge) |
| SFC (Super Fast Charging)    |      |      |        | [PD PPS](#usb-power-delivery)   |

### 無線
| 名称                       | 電力 |
|:--------------------------:|:----:|
| Fast Wireless Charging     | 9W   |
| Fast Wireless Charging 2.0 | ~15W |

## Xiaomi

### 有線
| 名称                    | 電圧 | 電流 | 電力 |
|:-----------------------:|:----:|:----:|:----:|
| 100W Super Charge Turbo |      |      | 100W |
| 120W Xiaomi HyperCharge |      |      | 120W |

### 無線
| 名称                                | 電力 |
|:-----------------------------------:|:----:|
| Mi Charge Turbo Wireless            | 30W  |
| 80W Mi Wireless Charging Technology | 80W  |
| 100W Mi Wireless Charging           | 100W |

## ASUS
| 名称                    | 電圧 | 電流 | 電力 |
|:-----------------------:|:----:|:----:|:----:|
| BoostMaster (QC2.0互換) | 9V   | 2A   | 18W  |

## Motolola TurboPower
| バージョン    | 電圧    | 電流        | 電力  | QC互換 |
|:-------------:|:-------:|:-----------:|:-----:|:------:|
| TurboPower 15 | 9 /12V  | 1.2/1.67A   | ~15W  | 2.0    |
| TurboPower 25 | 5/9/12V | 2.15/2.85A  | ~25W  | 2.0    |
| TurboPower 30 | 5V      | 5.7A        | 28.5W |        |

## OPPO VOOC (Voltage Open Loop Multi-step Constant-Current Charging)

### 有線
| 名称                             | 電圧 | 電流  | 電力  |
|:--------------------------------:|:----:|:-----:|:-----:|
| VOOC Flash Charge 2.0            | 5V   | 4A    | 20W   |
| VOOC Flash Charge 3.0            | 5V   | 5A    | 25W   |
| VOOC Flash Charge 4.0            | 5V   | 6A    | 30W   |
| SuperVOOC Flash Charge           | 10V  | 5A    | 50W   |
| SuperVOOC 2.0 Flash Charge       | 10V  | 6.5A  | 65W   |
| 125W flash charge (PPS Protocol) | 20V  | 6.25A | ~125W |
| 150W SUPERVOOC with BHE          | 20V  | 7.5A  | 150W  |
| 240W SUPERVOOC                   | 24V  | 10A   | 240W  |

### 無線
| 名称                       | 電力   |
|:--------------------------:|:------:|
| Wireless VOOC Flash Charge | 30W    |
| AirVOOC                    | 40/65W |
| MagVOOC                    | 20/40W |

## realme Dart

### 有線
| 名称                  | 電圧 | 電流  | 電力 |
|:---------------------:|:----:|:-----:|:----:|
| 30W Dart Charge       | 5V   | 6A    | 30W  |
| 65W Super Dart        | 10V  | 6.5A  | 65W  |
| 125W UltraDart charge | 20V  | 6.25A | 125W |

※ 65W Super Dartは[SuperVOOC 2.0 Flash Charge](#oppo-vooc-voltage-open-loop-multi-step-constant-current-charging)と互換性あり

### 無線
| 名称    | 電力   |
|:-------:|:------:|
| MagDart | 15/50W |

## OnePlus

### 有線
| 名称            | 電圧 | 電流 | 電力 |
|:---------------:|:----:|:----:|:----:|
| Dash Charge     | 5V   | 5A   | 25W  |
| Warp Charge 30T | 5V   | 6A   | 30W  |
| Warp Charge 65  | 10V  | 6.5A | 65W  |

### 無線
| 名称                    | 電力 |
|:-----------------------:|:----:|
| Warp Charge 15 Wireless | 15W  |
| Warp Charge 30 Wireless | 30W  |
| Warp Charge 50 Wireless | 50W  |

## vivo
| 名称              | 電圧 | 電流 | 電力 |
|:-----------------:|:----:|:----:|:----:|
| Super FlashCharge | 20V  | 6A   | 120W |

# 参考

<details>

[ユニバーサル・シリアル・バス - Wikipedia](https://ja.wikipedia.org/wiki/%E3%83%A6%E3%83%8B%E3%83%90%E3%83%BC%E3%82%B5%E3%83%AB%E3%83%BB%E3%82%B7%E3%83%AA%E3%82%A2%E3%83%AB%E3%83%BB%E3%83%90%E3%82%B9#USB%E7%B5%A6%E9%9B%BB)

[【USB】第3回 USBの充電仕様「USB Battery Charge」とは - @IT](https://www.atmarkit.co.jp/ait/articles/1806/01/news040.html)

[Battery Charging Specification 1.2(BC1.2)とは | 組込み技術ラボ](https://lab.fujiele.co.jp/articles/9966/)

[PoweredUSB 0.8g specification](http://www.poweredusb.org/pdf/PoweredUSB_v08g.pdf)

[PoweredUSB - Wikipedia](https://en.wikipedia.org/wiki/PoweredUSB)

[USB Charger (USB Power Delivery) | USB-IF](https://www.usb.org/usb-charger-pd)

[USB Type-C® Cable and Connector Specification Revision 2.1 | USB-IF](https://usb.org/document-library/usb-type-cr-cable-and-connector-specification-revision-21)

[USB Power Delivery and Type-C - STMicroelectronics](https://www.st.com/content/ccc/resource/sales_and_marketing/presentation/product_presentation/group0/5a/b1/8e/6c/2b/0d/46/3c/Apec/files/APEC_2016_USB_Power.pdf/_jcr_content/translations/en.APEC_2016_USB_Power.pdf)

[Qi (ワイヤレス給電) - Wikipedia](https://ja.wikipedia.org/wiki/Qi_(%E3%83%AF%E3%82%A4%E3%83%A4%E3%83%AC%E3%82%B9%E7%B5%A6%E9%9B%BB))

[Quick Charge - Wikipedia](https://ja.wikipedia.org/wiki/Quick_Charge)

[Introducing Qualcomm Quick Charge 3+, fast and efficient charging for the masses - Qualcomm](https://www.qualcomm.com/news/onq/2020/04/27/introducing-quick-charge-3-fast-and-efficient-charging-masses)

[Qualcomm Announces World’s Fastest Commercial Charging Solution, Quick Charge 5, World’s First Commercial 100W+ Charging Platform - Qualcomm](https://www.qualcomm.com/news/releases/2020/07/27/qualcomm-announces-worlds-fastest-commercial-charging-solution-quick-charge)

[Slide 2 - MediaTek](http://cdn-cw.mediatek.com/Features/Pump%20Express%20Series%20Introduction.pdf)

[iPhone 12 モデル以降で MagSafe 充電器を使う方法 - Apple サポート (日本)](https://support.apple.com/ja-jp/HT211829)

[VOOC - Wikipedia](https://en.wikipedia.org/wiki/VOOC)

[第840回：VOOC とは - ケータイ Watch](https://k-tai.watch.impress.co.jp/docs/column/keyword/1102448.html)

[OPPO VOOCチャージャー - 究極の充電体験 | オッポジャパン](https://www.oppo.com/jp/discover/technology/vooc/)

[急速充電および超急速充電（SuperCharge）に関するFAQ | HUAWEI サポート 公式サイト](https://consumer.huawei.com/jp/support/content/ja-jp00409980/)

[スマホ急速充電はついに125Wへ。OPPOが発表、4000mAhを20分で満充電に - Engadget 日本版](https://japanese.engadget.com/oppo-125w-flash-charge-030021166.html)

[What is Super Fast Charging? - The official Samsung Galaxy Site](https://www.samsung.com/global/galaxy/what-is/super-fast-charging/)

[What is Adaptive Fast Charging? - The official Samsung Galaxy Site](https://www.samsung.com/global/galaxy/what-is/adaptive-fast-charging/)

[What is Fast Wireless Charging 2.0? - The official Samsung Galaxy Site](https://www.samsung.com/global/galaxy/what-is/fast-wireless-charging/)

[120W Xiaomi HyperCharge for Xiaomi 11T Pro](https://www.mi.com/global/product/xiaomi-11t-pro-120w-xiaomi-hypercharge)

[Xiaomi Mi 100W Wireless Charging Stand launched for ¥599 ($92) - Gizmochina](https://www.gizmochina.com/2021/08/14/xiaomi-mi-100w-wireless-charging-stand-launched-for-%C2%A5599-92/)

[ZenFoneシリーズのQualcomm Quick Charge 2.0への対応状況を確認 : ASUS好きのZenBlog（ゼンブログ）](http://asus.blog.jp/archives/1052739551.html)

[Warp Charge 30TとDash Chargeの互換性をチェック - HANPEN-BLOG](https://hanpenblog.com/11976)

[OnePlus Warp Charge 65 Power Adapter](https://www.oneplus.com/product/oneplus-warp-charge-65-power-adapter)

[Warp Charge 50 Wireless Supercharger](https://www.oneplus.com/us/product/oneplus-warp-charge-50-wireless-charger)

[OPPO Flash Charging Technical Paper](https://www.oppo.com/content/dam/oppo/en/mkt/newsroom/story/flash-charging-technical-pape/OPPO%20Flash%20Charging%20Technical%20Paper.pdf)

[Oppo demonstrates MagVOOC magnetic charging adapters (40W and 20W) and a power bank - GSMArena.com news](https://www.gsmarena.com/oppo_demonstrates_magvooc_magnetic_charging_adapters_40w_and_20w_and_a_power_bank-news-50626.php)

[realme MagDart - realme (Malaysia)](https://www.realme.com/my/realme-mag-dart)

[Realme 65W SuperDart Charging: How Does it Work? Is it Safe For Your Phone?](https://gadgetstouse.com/blog/2020/10/24/realme-65w-superdart-charging-how-it-works-is-it-safe/)

[Realme introduces its own extreme fast charging - 125W UltraDART - GSMArena.com news](https://www.gsmarena.com/realme_also_introduces_extreme_fast_charging_calls_it_125w_ultradart-news-44292.php)

[OPPO、MWC2022にて複数の革新的な急速充電技術を発表 - オウガ・ジャパン](https://www.oppo.com/jp/newsroom/stories/oppo-mwc2022-supervooc/)
</details>
