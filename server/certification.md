# サーバー証明書の取得
## Cloudflare証明書

編集中

## Let's Encrypt + Certbot

[Certbotが公開している手順](https://certbot.eff.org/instructions)に従って導入する。  
サーバーソフトウェアとOSを選択するだけで詳しい手順を表示してくれる。かしこい。  
以下の手順は取得する証明書の種類に関わらず共通な手順なので、あらかじめ行っておきましょう。

1. snapdの導入を確認する。  
   
   ```
   sudo snap install core; sudo snap refresh core
   ```

2. certbotをインストールする。
   ```
   sudo snap install --classic certbot
   ```

3. `certbot`コマンドを有効にする。
   ```
   sudo ln -s /snap/bin/certbot /usr/bin/certbot
   ```

### ワイルドカード証明書

利用するDNSプロバイダーでワイルドカードの取得が可能かどうかは[公式ドキュメント](https://eff-certbot.readthedocs.io/en/stable/using.html#dns-plugins)に記載がある。

どのDNSプロバイダーを利用する場合でも、Certbotのプラグインをrootで動作可能にする必要がある。
```
sudo snap set certbot trust-plugin-with-root=ok
```

#### Cloudflare DNS

上記の手順は済ませていることを前提条件とする。  
公式ドキュメントでは[Install collect DNS plugin - wildcard](https://certbot.eff.org/instructions?ws=nginx&os=ubuntufocal)まで

1. Cloudflare DNSプラグインをインストールする。
   ```
   sudo snap install certbot-dns-cloudflare
   ```

2. CloudflareでAPIのCredentialを取得する。  
   [APIトークン](https://dash.cloudflare.com/profile/api-tokens)のページで「トークンを作成する」を押し、次の画面でAPIトークンテンプレートの「ゾーンDNSを編集する」の「テンプレートを使用」へ進む。

3. 「アクセス許可」が左から「ゾーン」、「DNS」、「編集」となっていることを確認する。

4. 「ゾーンリソース」で管理させるドメインを指定する。  
   一番左側はどのような設定であっても「包含」を選択する。  
   Cloudflare DNSで管理する全てのドメインの証明書を取得したい場合は「全てのゾーン」を、特定のドメインの証明書を取得したい場合には「特定のゾーン」を選択して管理したいドメインを指定する。  
   ※「アカウントにある全てのゾーン」を指定すると、通常アカウント上の全てのドメインがAPIアクセスの対象となる。チームでドメインを管理している状況で使う機能なので、通常使用することはない。
   ![Cloudflare APIトークンの作成](certification/cloudflare-api-credential.png)

5.  一番下の「概要に進む」を押し、証明書を取得したいドメインが表示されていることを確認して「トークンを作成する」を押す。

6. 次の画面で表示されたAPIトークンをコピーして、テキストエディタなどに一時的に保存しておく。  
   ![Cloudflare APIトークン](certification/cloudflare-api-token.png)

7. Credentialの保存場所を作成

   ```
   mkdir -p ~/.secrets/certbot
   ```

8. ファイルにCredentialを保存する。  
   `touch ~/.secrets/certbot/cloudflare.ini`で設定ファイルを作成する。  
   ファイルには以下に従って記述する。

   ```
   # Cloudflare API token used by Certbot
   dns_cloudflare_api_token = 先ほど保存したAPIトークン
   ```

9. 設定ファイルのパーミッションを変更して保護する。

   ```
   sudo chmod 600 ~/.secrets/certbot/cloudflare.ini
   ```

1. 証明書の発行テストを行う。
   ここではワイルドカード証明書を取得したいドメインを`*.example.com`とする。

   ```
   sudo certbot certonly \
   --dry-run \
   --dns-cloudflare \
   --dns-cloudflare-credentials ~/.secrets/certbot/cloudflare.ini \
   --dns-cloudflare-propagation-seconds 60 \
   -d *.example.com
   ```

   サブドメインなしのルートドメイン`example.com`も取得したい場合には、`-d example.com`もオプションに加える。  
   以降、指示に従ってメールアドレスの入力や規約への同意、メール配信の選択を行う。

2. 8の結果、`The dry run was successful.`と表示されたら、本番の証明書を取得する。

   ```
   sudo certbot certonly \
   --dns-cloudflare \
   --dns-cloudflare-credentials ~/.secrets/certbot/cloudflare.ini \
   --dns-cloudflare-propagation-seconds 60 \
   -d *.example.com
   ```

## 証明書の管理

証明書の一覧

```
sudo certbot certificates
```

証明書の更新

```
sudo certbot renew
```

自動更新の状態を確認

```
sudo systemctl list-timers
```

リストの`UNIT`が`snap.certbot.renew.timer`の項目があれば自動更新が有効

自動更新の有効化

```
sudo systemctl enable --now snap.certbot-renew.timer
```

自動更新のテスト

```
sudo certbot renew --dry-run
```

## 参考資料
[Let's Encrypt](https://letsencrypt.org/)

[Certbot](https://certbot.eff.org/)