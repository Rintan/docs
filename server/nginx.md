# nginxセットアップ

## 0. nginxのインストール

```
sudo apt install -y nginx
```

## 1. nginxの基本設定

設定ディレクトリの作成

```
sudo mkdir /etc/nginx/nginxconfig.io/
```

セキュリティ設定を記述する。

```
sudo touch /etc/nginx/nginxconfig.io/security.conf
sudo vim /etc/nginx/nginxconfig.io/security.conf
```

でファイルを作成し、以下の内容を貼り付けて保存する。

```
# security headers
add_header X-XSS-Protection          "1; mode=block" always;
add_header X-Content-Type-Options    "nosniff" always;
add_header Referrer-Policy           "no-referrer-when-downgrade" always;
add_header Content-Security-Policy   "default-src 'self' http: https: data: blob: 'unsafe-inline'; frame-ancestors 'self';" always;
add_header Permissions-Policy        "interest-cohort=()" always;
add_header Strict-Transport-Security "max-age=31536000; includeSubDomains" always;

# . files
location ~ /\.(?!well-known) {
    deny all;
}
```

プロキシ設定を記述する。

```
sudo touch /etc/nginx/nginxconfig.io/proxy.conf
sudo vim /etc/nginx/nginxconfig.io/proxy.conf
```

でファイルを作成し、以下の内容を貼り付けて保存する。

```
proxy_http_version                 1.1;
proxy_cache_bypass                 $http_upgrade;

# Proxy headers
proxy_set_header Upgrade           $http_upgrade;
proxy_set_header Connection        $connection_upgrade;
proxy_set_header Host              $host;
proxy_set_header X-Real-IP         $remote_addr;
proxy_set_header Forwarded         $proxy_add_forwarded;
proxy_set_header X-Forwarded-For   $proxy_add_x_forwarded_for;
proxy_set_header X-Forwarded-Proto $scheme;
proxy_set_header X-Forwarded-Host  $host;
proxy_set_header X-Forwarded-Port  $server_port;
```

nginxの基本設定を変更する。

```
sudo vim /etc/nginx/nginx.conf
```

でファイルを開き、以下の内容を`http`セクションに追記する。

```
http {
    # (前略)
    # Connection header for WebSocket reverse proxy
    map $http_upgrade $connection_upgrade {
        default upgrade;
        ""      close;
    }

    map $remote_addr $proxy_forwarded_elem {

        # IPv4 addresses can be sent as-is
        ~^[0-9.]+$        "for=$remote_addr";

        # IPv6 addresses need to be bracketed and quoted
        ~^[0-9A-Fa-f:.]+$ "for=\"[$remote_addr]\"";

        # Unix domain socket names cannot be represented in RFC 7239 syntax
        default           "for=unknown";
    }

    map $http_forwarded $proxy_add_forwarded {

        # If the incoming Forwarded header is syntactically valid, append to it
        "~^(,[ \\t]*)*([!#$%&'*+.^_`|~0-9A-Za-z-]+=([!#$%&'*+.^_`|~0-9A-Za-z-]+|\"([\\t \\x21\\x23-\\x5B\\x5D-\\x7E\\x80-\\xFF]|\\\\[\\t \\x21-\\x7E\\x80-\\xFF])*\"))?(;([!#$%&'*+.^_`|~0-9A-Za-z-]+=([!#$%&'*+.^_`|~0-9A-Za-z-]+|\"([\\t \\x21\\x23-\\x5B\\x5D-\\x7E\\x80-\\xFF]|\\\\[\\t \\x21-\\x7E\\x80-\\xFF])*\"))?)*([ \\t]*,([ \\t]*([!#$%&'*+.^_`|~0-9A-Za-z-]+=([!#$%&'*+.^_`|~0-9A-Za-z-]+|\"([\\t \\x21\\x23-\\x5B\\x5D-\\x7E\\x80-\\xFF]|\\\\[\\t \\x21-\\x7E\\x80-\\xFF])*\"))?(;([!#$%&'*+.^_`|~0-9A-Za-z-]+=([!#$%&'*+.^_`|~0-9A-Za-z-]+|\"([\\t \\x21\\x23-\\x5B\\x5D-\\x7E\\x80-\\xFF]|\\\\[\\t \\x21-\\x7E\\x80-\\xFF])*\"))?)*)?)*$" "$http_forwarded, $proxy_forwarded_elem";

        # Otherwise, replace it
        default "$proxy_forwarded_elem";
    }
}
```

## 2. nginxリバースプロキシの設定

サイト設定ファイルの作成

```
sudo touch /etc/nginx/sites-available/example.com.conf
sudo vim /etc/nginx/sites-available/example.com.conf
```

以下の内容を記述する。  
リバースプロキシするURLは`example.com`、内部ポートは`80`になっているので、環境に合わせて変更する。

```
server {
    listen                  443 ssl http2;
    listen                  [::]:443 ssl http2;
    server_name             example.com;

    # SSL
    ssl_certificate         /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key     /etc/letsencrypt/live/example.com/privkey.pem;
    ssl_trusted_certificate /etc/letsencrypt/live/exapmle.com/chain.pem;

    # security
    include                 nginxconfig.io/security.conf;

    # reverse proxy
    location / {
        proxy_pass http://127.0.0.1:80;
        include    nginxconfig.io/proxy.conf;
    }
}

# HTTP redirect
server {
    listen      80;
    listen      [::]:80;
    server_name example.com;

    location / {
        return 301 https://example.com$request_uri;
    }
}
```

シンボリックリンクの作成

```
sudo ln -s /etc/nginx/sites-available/example.com.conf /etc/nginx/sites-enabled
```

設定ファイルのテスト

```
sudo nginx -t
```

nginx設定の再読み込み

```
sudo nginx -s reload
```

## 参考資料
[NGINXConfig - DigitalOcean](https://www.digitalocean.com/community/tools/nginx)