# Linuxサーバーのセットアップ

サーバーをセットアップするときのメモ  
特に断りが無い場合の環境はUbuntu 20.04で検証を行っています。  
読者の対象は幅広いですが、検索などで自己解決の努力ができる人を想定しています。

## 目次

> - [0. サーバーに接続](#0-サーバーに接続)
> - [1. 作業ユーザーの作成](#1-作業ユーザーの作成)
> - [2. SSHの設定](#2-sshの設定)
>     - [鍵の生成と配置](#鍵の生成と配置)
>     - [公開鍵の転送](#公開鍵の転送)
> - [3. ファイアウォールの設定](#3-ファイアウォールの設定)
> - [4. サーバーにSSHで接続](#4-サーバーにsshで接続)
> - [クライアントのOpenSSHでクエリを設定(任意)](#クライアントのopensshでクエリを設定任意)
> - [fail2banの導入](#fail2banの導入)
> - [地域と言語(ロケール)の変更](#地域と言語ロケールの変更)

## 0. サーバーに接続

ホスティングサービスから提供される手段(VNCやWeb SSHクライアント)でrootにログインする。

## 1. 作業ユーザーの作成

1. ユーザーを作成する。  
   
   ```
   # adduser user
   ```

   `user`は任意のユーザー名に置き換える。  

   ※`useradd`は古く、ホームディレクトリが生成されないため使用しない。  
   ※環境によっては`useradd`が`adduser`にシンボリックリンクされており、同じコマンドとなる。

   ```
   (前略)
   Enter new UNIX password: # パスワードを設定
   Retype new UNIX password: # パスワードの確認
   passwd: password updated successfully
   Changing the user information for user
   Enter the new value, or press ENTER for the default
           Full Name []: # Enter
           Room Number []: # Enter
           Work Phone []: # Enter
           Home Phone []: # Enter
           Other []: # Enter
   Is the information correct? [Y/n] # Enter
   ```

2. 追加したユーザーの権限を変更する。
   
   ### `sudoers`を編集する方法

   `# visudo`で`/etc/sudoers.tmp`を開き、  
   以下のように追記して`user`に全てのコマンドの実行権限を与える。  

   ```
   # User privilege specification
   root ALL=(ALL:ALL) ALL
   user ALL=(ALL:ALL) ALL # この行を追加
   ```

   ### ユーザーが所属するグループを変更する方法

   作成した`user`を`sudo`グループに追加する。

   ```
   # gpasswd -a user sudo
   ```

   もしくは

   ```
   # usermod -aG sudo user
   ```

   CentOSでは`wheel`が`sudo`に相当するので

   ```
   # gpasswd -a user wheel
   ```

   もしくは

   ```
   # usermod -aG wheel user
   ```

   グループに追加せず、既存のグループの情報を上書きする。  

   ```
   # usermod -G sudo user
   ```

   > ユーザーの一覧を確認
   > 
   > ```
   > cat /etc/passwd
   > ```

   > ユーザーのグループを確認
   > 
   > ```
   > cat /etc/group
   > ```

   > ユーザー`user`の所属グループを確認]
   > 
   > ```
   > groups user
   > ```

3. 追加したユーザー`user`に切り替える。これ以降、追加したユーザーで作業する。
   
   ```
   su - user
   ```

   環境変数を引き継ぐ場合は

   ```
   su user
   ```

## 2. SSHの設定

### 鍵の生成と配置

1. クライアントのシェル(Powershellなど)で公開鍵と秘密鍵のペアを生成する。執筆時は4096ビット推奨 
 
   ```
   ssh-keygen -b 4096
   ```

2. `~/.ssh`(Windowsでは`C:\Users\<ユーザー名>\.ssh`)に秘密鍵`id_rsa`と公開鍵`id_rsa.pub`が生成されていることを確認する。これらの鍵を、外部ストレージなどのネットワークから隔離された安全で信頼性の高い場所に保存する。

### 公開鍵の転送

次の方法のうち、利用できる方法で公開鍵を転送する。  
これらの操作はクライアントから行う。

#### `ssh-copy-id`コマンドで公開鍵を転送する(クライアントがLinux限定)

ユーザー名`user`は先ほど設定したもの、IPアドレス`192.168.1.1`は環境に合わせて変更する。  
```
ssh-copy-id -i ~./ssh/id_rsa.pub user@192.168.1.1
```

#### sshで接続して、コピーペーストで公開鍵を転送する

ユーザー名`user`、ポート`22`、IPアドレス`192.168.1.1`は環境に合わせて変更する。

```
ssh -luser -p22 192.168.1.1
```

次の画面でSSH証明書を受け入れるかどうか聞かれるので`yes`と入力する。

サーバー側で公開鍵ファイルを作成する。

```
mkdir -p ~/.ssh && touch ~/.ssh/authorized_keys
```

作成した公開鍵をファイルに保存する。

```
vim ~/.ssh/authorized_keys
```

### サーバー側OpenSSHの設定

先ほど配置した公開鍵の権限を変更して保護する。

```
sudo chmod 700 ~/.ssh/ && sudo chmod 600 ~/.ssh/authorized_keys
```

(心配な場合は、設定ファイルをバックアップしておく。)

SSH設定ファイル`/etc/ssh/sshd_config`を編集する。

```
sudo vim /etc/ssh/sshd_config
```

以下のように変更する。

```

(前略)

Port 50000 #任意のポートに変更する
#Port 22
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::

(中略)

# Authentication:

#LoginGraceTime 2m
PermitRootLogin no #noに変更
#StrictModes yes
#MaxAuthTries 6
#MaxSessions 10

(中略)

# To disable tunneled clear text passwords, change to no here!
PasswordAuthentication no ##を外してnoに変更
#PermitEmptyPasswords no

(後略)
```

## 3. ファイアウォールの設定

ufwのステータスを確認する。

```
sudo ufw status
```

`Status: active`になっていたら、一時的に無効にする。

```
sudo ufw disable
```

デフォルトのSSHポートを拒否して、代わりに先ほど設定したポート(ここでは`50000`)を許可する。

```
sudo ufw deny ssh
sudo ufw allow 50000/tcp
```

ufwを有効にして、指定したポートが許可されているかを確認する。

```
sudo ufw enable
sudo ufw status
```

sshdを再起動する。

```
sudo service sshd restart
```

SSHが接続できることを確認する。[サーバーにSSHで接続する](#サーバーにSSHで接続する)を参照

最後にufwを再読み込みする。

```
sudo ufw reload
```

ここまでで最低限のサーバーのセットアップが完了。

## 4. サーバーにSSHで接続

`-i`で秘密鍵の場所を指定し、`-p`で設定したSSHのポートを指定する。  
Windowsでは秘密鍵は`C:\Users\<ユーザー名>\.ssh\id_rsa`

```
ssh -i ~/.ssh/home/id_rsa -p 50000 user@192.168.1.1
```

## クライアントのOpenSSHでクエリを設定(任意)

Windowsでは`C:\Users\<ユーザー名>\.ssh\config`を編集する(ない場合は新規に作成する)

以下を追記する。

```
Host server #分かりやすい名前を付ける
   HostName 192.168.1.1 #サーバーのIPアドレス(IPv4でもv6でもいい)
   User user #ログインするユーザー名
   Port 50000 #設定したSSHポート
   PreferredAuthentications publickey
   StrictHostKeyChecking no
   PasswordAuthentication no
   IdentityFile C:/Users/<ユーザー名>/.ssh/id_rsa #秘密鍵の場所
   IdentitiesOnly yes
   LogLevel FATAL
```

## fail2banの導入

不審なアクセスからサーバーを守ります。

```
sudo apt install fail2ban
```

## 地域と言語(ロケール)の変更

日本語パッケージをインストールする。

```
sudo apt install language-pack-ja-base language-pack-ja
```

ロケールを日本語にする。

```
sudo update-locale LANG=ja_JP.UTF8
```

manコマンドの日本語マニュアルを導入する。

```
sudo apt install manpages-ja manpages-ja-dev
```

再起動する。

```
sudo reboot
```

タイムゾーンを変更する。

```
sudo dpkg-reconfigure tzdata
```

グラフィカルな画面が出てくるので、矢印キーを操作してアジア > 東京を選ぶ。

## 参考資料

[【Ubuntu】ユーザ管理の方法について、ちゃんと調べてみた - とーますメモ](https://thoames.hatenadiary.jp/entry/2020/05/22/143845)