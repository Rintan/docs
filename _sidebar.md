<!-- _sidebar.md -->
<div style="text-align: center">Documents Tree<br>by Rintan</div>

 - [ホーム](/)

 - テクノロジー
   - [USB・無線充電規格](tech/charging.md)
 - サーバー
   - [初期セットアップ](server/setup.md)
   - [サーバー証明書の取得](server/certification.md)